
/* A little engine that typed */
var textHolder, textTarget, letter, a = 0, ii = 0 , li, typeSpeed = 25,
	shortPause = 450,
	longPause = 700,
	tenseSilence = 2000,
	waiting = false,
	em = false,
	bold = false;

_type = function(text_target, text) {
	if (waiting == false) {
		li = 0;
		a += 1;
		console.log(a);
		waiting = true;
		textTarget = text_target;
		textTarget.className = 'wake';
		//textTarget.removeEventListener('click', _type, false);
		textTarget.innerHTML = '';
		//textTarget.className = '';
		textHolder = text.split('');
		_get();
	}
};

_get = function() {
	if (li < textHolder.length) {
		setTimeout(
			function() {
				letter = document.createTextNode(textHolder[li]);
				_print(textTarget, letter);
			}, typeSpeed);

	} else {
		waiting = false;
		// funcs[0]('');
	}
};

_print = function(textTarget, letter) {
	li++;
	if (letter.nodeValue == ',' || letter.nodeValue == ';' || letter.nodeValue == ':') {
		setTimeout(_get, shortPause);
	} else if (letter.nodeValue == '.' || letter.nodeValue == '!' || letter.nodeValue == '?') {
		setTimeout(_get, longPause);
	} else if (letter.nodeValue == '_') {
		letter.nodeValue = '<br>';
		setTimeout(_get, longPause);
	} else if (letter.nodeValue == '^') {
		/*inserts the letter in a span*/
		letter.nodeValue = '';
		if(em == true) em = false;
		else em = true;
		_get();
	} else if (letter.nodeValue == '>') {
		/* run next function */
		// waiting = false;
		// letter.nodeValue = '';
		// ii++;
		// setTimeout(_type(funcs[ii][0],funcs[ii][1]), tenseSilence);
		/* execute fade in effect */
		letter.nodeValue = '';
		$("#a" + a.toString()).animate({ opacity: '1.0'},1000);
		$("#b" + a.toString()).animate({ opacity: '1.0'},2000);
		_get();
	} else if (letter.nodeValue == '*') {
		/* bold these letters */
		letter.nodeValue = '';
		if(bold == true) bold = false;
		else bold = true;
		_get();
	} else {
		_get();
	}
	if(em == true) $(textTarget).append('<span>'+ letter.nodeValue +'</span>');
	else if(bold == true) $(textTarget).append('<strong>'+ letter.nodeValue +'</strong>');
	else $(textTarget).append(letter.nodeValue);
};
var funcs = [
	// [document.getElementById('title'), "High Ticket Closing....>"]
];

_type(document.getElementById('q1'), "To help us better understand your needs, what type(s) of assets are you working with?>");
//  $('#nextbtn-1').click(function () {
// 	_type(document.getElementById('q2'), "We can definitely help with that! How many assets do you have?");
//  });
// $('#headline').on('inview', _type(document.getElementById('headline'), "Would You Like to ^Double, Triple, or Even Quadruple^ Your Sales Closing Ratios Without Ever Doing the Selling Yourself?>"));
// $('#opening').on('inview', _type(document.getElementById('opening'), "I’m looking for a “high-quality client” that I can help close qualified leads and generate consistent sales that result into higher profits and lifetime clients.__By consistent sales and results, I mean having me be your go-to sales closer, where I’ll help you double, triple, or even quadruple your sales closing ratios, without you ever having to speak to your prospects.__However, pay special attention to this message I’m about to share with you.__I’m am here because I want to attract “A” level clients who want to close their “High-Ticket” services in less time and in just one phone call, without having to do the selling themselves.__But first…__"));
// _type(document.getElementById('title'), "High Ticket Closing>");