
var app = new Vue({
    el: '#app',
    mounted:function(){
      this.populate() //method1 will execute at pageload
    },
    data: {
      showInvestmentDetails: true,
      showFleetDetails: false,
      showSafetyDetails: false,
      
      years: 1,
      
      code: "xyz",  
      nv: 1, // number of vehicles
      tc: 39, // cost per vehicle
      vt: "Heavy Vehicles", // vehicle type
      
      a1: 60,
      b1: 20,
      c1: 20,
      d1: 0.9,
      e1: 2.5,
      f1: 380,

      a2: 2500,
      b2: 6.5,
      c2: 4,

      a3: 12,
      b3: 1,
      c3: 2,
      d3: 32,
      e3: 50,

      a4: 15,
      b4: 15,
      c4: 30,
      d4: 24,

      a5: 20,

      a6: 0.1,
      b6: 66351,
      c6: 25,
      d6: 10

      



    },
    methods: {
      setLicensePeriod: function(period){
        this.years = period;
        this.populate();
      },

      vehicleTypeChanged: function(){
        if(this.vt == "Cars"){
          this.d1 = 0.2;
          this.f1 = 93;
          this.b2 = 26;
          this.b6 = 15000;
        } else if (this.vt == "Light Trucks") {
          this.d1 = 0.8;
          this.f1 = 175;
          this.b2 = 18;
          this.b6 = 66351;
        } else if (this.vt == "Heavy Vehicles") {
          this.d1 = 0.9;
          this.f1 = 380;
          this.b2 = 6.5;
          this.b6 = 66351;
        }
      },
      populate: function(){
        var url = window.location.href;
        if(url.includes("?")){
          var info = url.split("?")[1];
          if(info.includes("&")){
            var strnv = info.split("&")[0];
            var codes = info.split("&")[1];
            console.log(info[0] + " " +info[1]);
            this.nv = parseInt(strnv);
            var code = codes.split(",");
            if(code.includes("4")||code.includes("6")){
              this.vt = "Heavy Vehicles";
            }
            else if(code.includes("2")||code.includes("3")){
              this.vt = "Light Trucks";
            }
            else if(code.includes("1")||code.includes("5")){
              this.vt = "Cars";
            }
            else{
              this.vt = "Heavy Vehicles";
            }
          }
        } 
      }
    },
    
    computed: {
      nm(){
        return this.years * 12;
      },
      i1(){
        return (((this.c1 * this.nm * this.nv * this.d1 * this.e1) * (this.a1 / 60)) - ((this.c1 * this.nm * this.nv * this.d1 * this.e1) * (((this.a1 - ((this.b1 * this.a1) / 100))) / 60)));
      },
      i2(){
        return Math.round((this.nm * this.nv * this.e1 * (this.a2 / this.b2)) - (this.nm * this.nv * this.e1 * (((this.a2 - ((this.c2 * this.a2)/ 100))) / this.b2)));
      },
      i3(){
        return (((this.nm / this.a3) * this.nv * this.b3 * this.c3 * this.d3) - ((this.nm / this.a3) * (this.e3 / 100) * this.nv * this.b3 * this.c3 * this.d3));
      },
      i4(){
        return (((52 / (60 / this.a4)) * this.d4) * this.years * this.nv + ((52  / (60 / this.b4)) * this.c4) * this.years * this.nv);
      },
      i5(){
        return (this.a5 * this.nm * this.nv);
      },
      i6(){
        return (this.f1 * this.nm * this.nv);
      },
      i7(){
        return Math.ceil(this.i8 * this.b6);
      },
      i8(){
        
        return ((this.nv * this.a6) / 100) * (parseInt(this.c6) + parseInt(this.d6));
      },
      a(){
        return (this.nm * this.tc * this.nv);
      },
      b(){
        return (this.i1 + this.i2 + this.i3 + this.i4 + this.i5 + this.i7 + this.i8);
      },
      c(){
        return (this.b - this.a);
      },
      d(){
        return Math.round(this.a / (this.b / 12));
      },
      e(){
        return Math.round((this.c / this.a) * 100);
      },


      i1_s(){
        return (((this.c1 * this.nm * this.nv * this.d1 * this.e1) * (this.a1 / 60)) - ((this.c1 * this.nm * this.nv * this.d1 * this.e1) * (((this.a1 - ((this.b1 * this.a1) / 100))) / 60))).toLocaleString();
      },
      i2_s(){
        return Math.round((this.nm * this.nv * this.e1 * (this.a2 / this.b2)) - (this.nm * this.nv * this.e1 * (((this.a2 - ((this.c2 * this.a2)/ 100))) / this.b2))).toLocaleString();
      },
      i3_s(){
        return (((this.nm / this.a3) * this.nv * this.b3 * this.c3 * this.d3) - ((this.nm / this.a3) * (this.e3 / 100) * this.nv * this.b3 * this.c3 * this.d3)).toLocaleString();
      },
      i4_s(){
        return (((52 / (60 / this.a4)) * this.d4) * this.years * this.nv + ((52  / (60 / this.b4)) * this.c4) * this.years * this.nv).toLocaleString();
      },
      i5_s(){
        return (this.a5 * this.nm * this.nv).toLocaleString();
      },
      i6_s(){
        return (this.f1 * this.nm * this.nv).toLocaleString();
      },
      i7_s(){
        return Math.ceil(this.i8 * this.b6).toLocaleString();
      },
      i8_s(){
        
        return Math.round(((this.nv * this.a6) / 100) * (parseInt(this.c6) + parseInt(this.d6))).toLocaleString();
      },
      a_s(){
        return Math.round(this.nm * this.tc * this.nv).toLocaleString();
      },
      b_s(){
        return Math.round(this.i1 + this.i2 + this.i3 + this.i4 + this.i5 + this.i7 + this.i8).toLocaleString();
      },
      c_s(){
        return Math.round(this.b - this.a).toLocaleString();
      },
      d_s(){
        return Math.round(this.a / (this.b / 12)).toLocaleString();
      },
      e_s(){
        return Math.round((this.c / this.a) * 100).toLocaleString();
      }

    }
  })




